[Galaxie-Clans Documentation](README.md) / [Users Guides](_UREF__.md)

# Valider les pré-requis de votre machine

## Navigateur web

La plateforme de vidéoconférence Jitsi. Pour un confort d'utilisation maximal, nous vous conseillons d'utiliser un navigateurs basé sur Chrome :

* [Chrome](https://www.google.com/chrome/) : version 86.0.4240 ou supérieure
* [Chromium](https://www.chromium.org/) : version 86.0.4240 ou supérieure
* [Brave](https://brave.com/) : version 1.16.67 ou supérieure