# Galaxie-Clans Documentation

![galaxie](img_galaxie_logo.png)

## How is the documentation organized ?

* [Users Guides](_UREF__.md) 
    * Describe __how things work and how to use them__ but assume that you have a basic understanding of key concepts. Also contain technical reference on internals.
* [Tutorials](_TUTO__.md) 
    * __Start here__ if you’re new to Galaxie-Clans. Tutorials take you by the hand through a series of steps to achieve results with Galaxie-Clans.
* [How-To Guides](_HOWTO__.md)
    * Address __key problems and specific use-cases__. They are more advanced than tutorials and assume some knowledge of how Galaxie-Clans works.
* [Reference Guides](_REF__.md) 
    * Describe __how things work and how to use them__ but assume that you have a basic understanding of key concepts. Also contain technical reference on internals.
* [Topic Guides](_TOPIC__.md) 
    * Provide useful __background information and explanation__ and discuss key topics and concepts at a fairly high level.

## Why is it so ?

This structure is borrowed from Daniele Procida's presentation at PyCon Australia: "What nobody tells you about documentation".

Blessed be he for his teachings.

[Learn more...](https://documentation.divio.com/)