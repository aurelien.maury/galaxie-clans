[Galaxie-Clans Documentation](README.md) / [How-to Guides](_HOWTO__.md)

# How to update a user's password ?

## Ready?

The target host must be one of `galaxie-clans`.

## Set.

* `$INVENTORY_HOSTNAME` is the name of the target host
* `$UNAME` should match the `uname` of a user managed by `system_users` role

## Go!

### Get password hash

The user must, on his side, use this command to hash the password he likes:
```
mkpasswd -m SHA-512
```
...and give it to the clan caretaker. 

> Until the end, this user-supplied hash value will be referenced as `$UHASH`.

### Store password hash

Run:
```
ansible-playbook playbooks/ops_store_user_hash.yml \
    -e scope=$INVENTORY_HOSTNAME \
    -e uname=$UNAME \
    -e uhash='$UHASH'
```

>
> Beware of the single quotes around `$UHASH`.
>

### Spread password hash value

Run:
```
ansible-playbook playbooks/setup_core_services.yml -e scope=$INVENTORY_HOSTNAME
```

If the clan host has some of broadcast services enabled, also run:
```
ansible-playbook playbooks/setup_broadcast_services.yml -e scope=$INVENTORY_HOSTNAME 
```

## EOC