[Galaxie-Clans Documentation](README.md) / [Tutorials](_TUTO__.md)

# Tutorial name

## Ready?

Put here the prerequisites for the how-to.

## Set.

Inform the reader about some writing conventions or context informations.

## Go!

### Step

* __substep 1__
* __substep 2__
* __substep 3__

## EOC