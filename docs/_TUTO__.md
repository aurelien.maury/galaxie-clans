[Galaxie-Clans Documentation](README.md)

# Tutorial Guides

* [Found a galaxie-clan on a Kimsufi server](_tuto_found_clan_ovh.md)
* [Prepare a Scaleway Dedibox server for installation](_tuto_prepare_dedibox.md)