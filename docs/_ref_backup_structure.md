[Galaxie-Clans Documentation](README.md) / [Reference Guides](_REF__.md)

# Backup Structure

## Directory structure

```
backups/
  {{ inventory_hostname }}/
    {{ role_name }}/
      {{ date }}/
```
