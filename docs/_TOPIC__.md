[Galaxie-Clans Documentation](README.md)

# Topic Guides

## Necessary introduction

### Over the top

* Debian: because it's "The Universal Operating System"
* Ansible: because it's "Simple, agentless IT automation that anyone can use"
* Every component should be open-source, popular and have a big contributor community

### Release naming

Major release are named after the runes names of the elder Futhark. 

* 1.X.X FEHU

### Color Palette

* Black `#001516`
* Grey `#ccccccff`
* Cyan `#8ef4f7`
* Cyan strong `#6bd1d4`
