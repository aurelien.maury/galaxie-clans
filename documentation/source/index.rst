*************
Galaxie-Clans
*************

.. image:: _static/img/big_picture.png
  :width: 100%
  :alt: Big Picture

Structure
=========

This documentation structure is borrowed from Daniele Procida's presentation at PyCon Australia: "What nobody tells you about documentation".

Blessed be he for his teachings. `Learn more... <https://documentation.divio.com/>`_

.. toctree::
   :maxdepth: 1

   tutorials
   howto
   explanation
   reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
