*******************
Once Upon a time...
*******************

...this project was hosted on a ancient platform called GitHub. 
Then came the Buyer. The Buyer bought GitHub, willing to rule over its community. 
We were not to sell, so here is the new home of https://github.com/Tuuux/galaxie 
and the project has much mutated since then...

We built an Ansible toolkit for managing a set of Debian servers, a set of 
powerful roles made hand-in-hand by a senior system admin and a senior developer.

Originaly written for managing a Home Network with external services. The project 
has shifted to managing digital survival of clanic entities.

Galaxie-Clans is a true mix of the two world, by luck author Tuuux and Mo have made the 
necessary to have no compromise.

.. admonition:: PRO TIPS
    :class: important

    Tech purity is cancer for the mind.

It would be an honor if you could use our work in any way. Be it as-is or as code samples.

Having fun since 2015-03-21, hack in peace.
